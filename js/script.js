let targetValue = 10;
let intervalID;
let allImages = document.querySelectorAll('.image-to-show');
let activeImage = 0;
let btnStart = document.getElementById('button-start');
let btnStop = document.getElementById('button-stop');
let isPaused = false;


function startTimer() {
    intervalID = setInterval(()=>{
        if (isPaused) {
            return
        }
        if (targetValue < 0) {
            targetValue = 10;
            clearInterval(intervalID);
            updateImage();
            startTimer(10);
        }
        if (++activeImage === allImages.length) {
            activeImage = 0;
        };
        console.log(targetValue--);
    }, 1000);
}

function updateImage() { allImages.forEach((image, index)=>{
    if (index === activeImage) {
        image.style = 'display: flex;';
        return;
    }
    image.style = 'display: none;';
});
}

btnStart.addEventListener('click', ()=>{
    isPaused = false;
    startTimer(10);
});

btnStop.addEventListener('click', ()=>{
    isPaused = true;
});

startTimer(targetValue);
updateImage();
